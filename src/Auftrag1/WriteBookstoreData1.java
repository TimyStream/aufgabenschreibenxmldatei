package Auftrag1;
/* Arbeitsauftrag:  Erstellen Sie ein DOM-Dokument gem�� den Vorgaben 
 * 					aus der Datei "Vorgabe_f�r_Ausgabedatei.xml" 
 * 					und sichern Sie es als XML in eine Datei 
 * 					mit dem Filename "buchhandlung.xml".
 * 	
 *               
 */

import org.w3c.dom.*;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class WriteBookstoreData1 {

	public static void main(String[] args) {

		try {
			
			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = builderFactory.newDocumentBuilder();
			Document document = dBuilder.newDocument();

			Element buchhandlung = document.createElement("buchhandlung");
			document.appendChild(buchhandlung);

			Element buch = document.createElement("buch");
			buchhandlung.appendChild(buch);

			Element titel = document.createElement("titel");
			buch.appendChild(titel);
			titel.setAttribute("lang", "de");
			titel.appendChild(document.createTextNode(" Java ist auch eine Insel "));

			TransformerFactory tFactory = TransformerFactory.newInstance();
			Transformer transformer = tFactory.newTransformer();

			DOMSource source = new DOMSource(document);

			StreamResult result = new StreamResult(new File("buchhandlung.xml"));

			transformer.transform(source, result);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
