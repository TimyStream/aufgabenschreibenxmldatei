package Auftrag3;
/* Arbeitsauftrag:  Speichern Sie die Liste der B�cher 
 * 					(buchliste) in die Datei "buchhandlung.xml".
 * 					Dabei gehen Sie wie folgt vor:
 *                  - Erstellen Sie ein DOM-Dokument  
 * 					- sichern Sie es als XML in die Datei "buchhandlung.xml".
 * 
 * 	Hinweis: Die Struktur der Ergebnisdatei soll der Datei 
 *           "Vorgabe_f�r_Ausgabedatei.xml" entsprechen. 			
 *               
 */

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class WriteBookstoreData3 {

	public static void main(String[] args) {

		List<Buch> buchliste = new ArrayList<>();
		buchliste.add(new Buch("Everyday Italian", "Giada De Laurentiis", 30.0));
		buchliste.add(new Buch("Harry Potter", "J K. Rowling", 29.99));
		buchliste.add(new Buch("XQuery Kick Start", "James McGovern", 49.99));
		buchliste.add(new Buch("Learning XML", "Erik T. Ray", 39.95));

		//Add your code here
		try {
			DocumentBuilderFactory dBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dBuilderFactory.newDocumentBuilder();
			Document document = dBuilder.newDocument();

			Element buchhandlung = document.createElement("buchhandlung");
			document.appendChild(buchhandlung);

			for (int i = 0; i<4; i++) {
				Element buch = document.createElement("buch");
				buchhandlung.appendChild(buch);

				Element titel = document.createElement("titel");
				titel.appendChild(document.createTextNode(buchliste.get(i).getTitel()));

				buch.appendChild(titel);

				Element autor = document.createElement("autor");
				autor.appendChild(document.createTextNode(buchliste.get(i).getAutor()));
				
				buch.appendChild(autor);

				Element preis = document.createElement("preis");
				preis.appendChild(document.createTextNode(String.valueOf(buchliste.get(i).getPreis())));

				buch.appendChild(preis);
			}

			TransformerFactory tFactory = TransformerFactory.newInstance();
			Transformer transformer = tFactory.newTransformer();

			DOMSource source = new DOMSource(document);

			StreamResult result = new StreamResult(new File("buchhandlung.xml"));

			transformer.transform(source, result);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
